package com.example.androidstudio.prelimmillonte1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity1 extends AppCompatActivity {
    int num = 0, start = 1, end =10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);

        Button pass = (Button) findViewById(R.id.btnCompute);

        pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView ans = (TextView) findViewById(R.id.lblAnswer);

                for (int x = start; x <= end; x++) {
                    num += x;
                    ans.append(x + " = " + num + "\n" );
                }
            }
        });
    }
}
