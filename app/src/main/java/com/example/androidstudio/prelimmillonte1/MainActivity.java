package com.example.androidstudio.prelimmillonte1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    Random r = new Random();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final int n = r.nextInt(10);
        final EditText num = (EditText) findViewById(R.id.txtInteger);
        Button tryy = (Button) findViewById(R.id.btnTry);
        Button reveal = (Button) findViewById(R.id.btnReveal);


        tryy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int no = Integer.parseInt(String.valueOf(num.getText()));

                if (no > n) {
                    Toast.makeText(getApplicationContext(), "It's Lower", Toast.LENGTH_SHORT).show();
                }

                else if (no < n) {
                    Toast.makeText(getApplicationContext(), "It's Higher", Toast.LENGTH_SHORT).show();
                }

                else {
                    Toast.makeText(getApplicationContext(), "Conratumalations!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        reveal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nu = String.valueOf(n);
                TextView noo = (TextView) findViewById(R.id.lblNum);
                noo.setText(nu);
            }
        });


    }

}
